/* eslint linebreak-style: ["error", "windows"] */

import express from 'express';
import middleware from './middleware/index';
import './database/index';

import CategoryRoutes from './modules/category/routes';
import ProductRoutes from './modules/products/routes';
import CustomerRoutes from './modules/customer/routes';

const app = express();
const PORT = 3000;

// All the middleware are added
middleware(app);

// All the routes are added here

app.use('/api/product', ProductRoutes);
app.use('/api/category', CategoryRoutes);
app.use('/api/customer', CustomerRoutes);

// start the server
app.listen(PORT, (error) => {
  if (error) {
    console.error(error);
  }
  console.log(`Server started on ${PORT}`);
});

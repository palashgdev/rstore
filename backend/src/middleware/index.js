/* eslint linebreak-style: ["error", "windows"] */

import express from 'express';
import morgan from 'morgan';
import { isDev } from '../config/index';

export default (app) => {
  // logger for http request
  app.use(morgan(isDev ? 'dev' : 'common'));

  // body parser alternative
  app.use(express.json());
};


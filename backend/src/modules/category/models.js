import mongoose, { Schema } from 'mongoose';

const CategoryScehma = new Schema({
  name: {
    type: String,
    unique: true,
  },
  products: {
    type: [Schema.Types.ObjectId],
    ref: 'Product',
  },
});
CategoryScehma.index({ name: 1 });

// products.find({ _id: { $in: [ObjectId('5b9b9abcf10b6b13105f9495'), ObjectId('5ba1416e0c152a2e1c9dbdba'), ObjectId('5ba1416e0c152a2e1c9dbdbb')] } }, { fields: { name: 1, price: 1 } });


export default mongoose.model('Category', CategoryScehma);

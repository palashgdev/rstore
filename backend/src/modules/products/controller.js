/**
 * This file contain the all the database function realted to the controller
 */

import * as Yup from 'yup';

import {
  createProduct, getProduct, getAllProduct, deleteProduct, updateProduct,
} from './product';

export const createProducts = async (req, res) => {
  try {
    const {
      name, price, description, productAvatar,
    } = req.body;

    const bodySchema = Yup.object().shape({
      name: Yup.string().required(),
      price: Yup.number().required(),
      description: Yup.string().required(),
      productAvatar: Yup.string().required(),
    }).required();

    await bodySchema.validateSync({
      name, price, description, productAvatar,
    });

    const products = await createProduct(
      name,
      description,
      price,
      productAvatar,
    );

    res.json(products);
  } catch (error) {
    res.json({ message: error });
  }
};

export const getproduct = async (req, res) => {
  try {
    const { productId: name } = req.params;

    const bodySchema = Yup.object().shape({
      name: Yup.string().required().strict(true),
    });

    await bodySchema.validateSync({ name });

    const products = await getProduct(name);

    res.json({ response: products });
  } catch (error) {
    res.json({ message: error });
  }
};

export const getAllProducts = async (req, res) => {
  try {
    const products = await getAllProduct();

    res.json({ response: products });
  } catch (error) {
    res.json({ message: error });
  }
};

export const updateProducts = async (req, res) => {
  try {
    const {
      name, price, description, productAvatar, updatedName,
    } = req.body;

    const bodySchema = Yup.object().shape({
      name: Yup.string().strict(true),
      price: Yup.number().notRequired(),
      updateName: Yup.string().notRequired(),
      description: Yup.string().strict(true).notRequired(),
      productAvatar: Yup.string().strict(true).notRequired(),
    });

    await bodySchema.validateSync({
      name, price, description, productAvatar,
    });

    const _products = await updateProduct(name, {
      updatedName, price, description, productAvatar,
    });

    res.json({ response: _products });
  } catch (error) {
    res.json({ message: error });
  }
};

export const deleteProducts = async (req, res) => {
  try {
    const { productId: name } = req.params;

    console.log(req.params);


    const bodySchema = Yup.object().shape({
      name: Yup.string().required().required(),
    });

    await bodySchema.validateSync({ name });

    const product = await deleteProduct(name);

    res.json({ response: product });
  } catch (error) {
    res.json({ message: error });
  }
};

/**
 * This file contain the Schema of the products
 */
import mongoose, { Schema } from 'mongoose';

const ProductSchema = new Schema({
  name: {
    type: String,
    unique: true,
  },
  productAvatar: [String],
  price: Number,
  description: String,
  reveiw: {
    required: false,
    type: [Schema.Types.ObjectId],
    ref: 'reveiw',
  },
}, { timestamps: true });

ProductSchema.index({ name: 1 });

export default mongoose.model('Product', ProductSchema);

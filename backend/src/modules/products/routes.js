import { Router } from 'express';

import {
  createProducts,
  getproduct,
  getAllProducts,
  updateProducts,
  deleteProducts,
} from './controller';


const Routers = Router();

Routers.post('/', createProducts);
Routers.get('/:productId', getproduct);
Routers.get('/', getAllProducts);
Routers.put('/', updateProducts);
Routers.delete('/:productId', deleteProducts);

export default Routers;


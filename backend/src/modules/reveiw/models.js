import mongoose, { Schema } from 'mongoose';

const ReveiwSchema = new Schema({
  product: {
    type: Schema.Types.ObjectId,
    ref: 'product',
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
  },
  description: {
    type: String,
    required: true,
  },
  star: Number,
});

ReveiwSchema.index({ product: 1 });

export default mongoose.model('reveiw', ReveiwSchema);

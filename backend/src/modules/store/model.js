import mongoose, { Schema } from 'mongoose';


const StoreSchema = new Schema({
  storeOwner: {
    type: String,
  },
  storeName: {
    type: String,
    required: true,
    unique: true,
  },
  storeIcon: {
    type: String,
    required: true,
  },
  category: [Schema.Types.ObjectId],
});

StoreSchema.index('storeName', true);

export default mongoose.model('store', StoreSchema);


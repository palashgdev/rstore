/* eslint linebreak-style: ["error", "windows"] */

import { facebook } from './facebook';
import { google } from './google';

export const AuthProvider = {
  facebook,
  google,
};

import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigation(ref) {
  _navigator = ref;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}


function back() {
  _navigator.dispatch(NavigationActions.back());
}

function popToTop(immediate = true) {
  _navigator.dispatch({
    type: NavigationActions.popToTop,
    immediate,
  });
}

function reset({ actions, index }) {
  _navigator.dispatch({
    type: NavigationActions.RESET,
    index,
    actions,
  });
}

export const NavigatorServices = {
  navigate,
  setTopLevelNavigation,
  back,
  popToTop,
  reset,
  navigator: _navigator,
};

// making to access anywhere
window.NavigatorServices = NavigatorServices;

/* eslint linebreak-style: ["error", "windows"] */
import { Facebook, Constants } from 'expo';

const permissons = ['public_profile', 'email'];

const loginAsync = async () => {
  try {
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      Constants.manifest.facebookAppId,
      {
        permissions: permissons,
      },
    );

    if (type === 'success') {
      return Promise.resolve(token);
    }
    return Promise.reject('Failed');
  } catch (error) {
    return Promise.reject(error);
  }
};

export const FacebookApi = {
  loginAsync,
};

/* eslint linebreak-style: ["error", "windows"] */
import { Google, Constants } from 'expo';

const scope = ['profile', 'email'];

const loginAsync = async () => {
  try {
    const res = await Google.logInAsync({
      androidClientId: Constants.manifest.extra.googleAppId.android,
      iosClientId: Constants.manifest.extra.googleAppId.ios,
      scopes: scope,
      androidStandaloneAppClientId: '526477970273-n42a2m8dojl4h332d3b7s017tp7q79kn.apps.googleusercontent.com',
      iosStandaloneAppClientId: '526477970273-91loq23bbej5iduu79cbbrldc6nlsulm.apps.googleusercontent.com',
      webClientId: '526477970273-n42a2m8dojl4h332d3b7s017tp7q79kn.apps.googleusercontent.com',
    });

    if (res.type === 'success') {
      return Promise.resolve(res.accessToken);
    }

    return Promise.reject('Google Login Failed');
  } catch (error) {
    return Promise.reject(error);
  }
};

export const googleApi = {
  loginAsync,
};

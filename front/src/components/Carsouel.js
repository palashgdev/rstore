/* eslint linebreak-style: ["error", "windows"] */

import React, { PureComponent } from 'react';
import {
  Image, ScrollView, Dimensions,
} from 'react-native';
import { Box } from 'react-native-design-utility';

const { width: WIDTH } = Dimensions.get('window');

const images = [
  require('../../assets/images/sale.png'),
  require('../../assets/images/sale.png'),
  require('../../assets/images/sale.png'),
  require('../../assets/images/sale.png'),
];

const DOT_SIZE = 8;
const TIME = 3000;

class Carsiuel extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
    };

    this.scrollView = React.createRef();
  }

  componentDidMount() {
    this.timer = setInterval(() => {
      this.handleScroll();
    }, TIME);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  handleScroll = () => {
    const { currentIndex } = this.state;
    const newIndex = currentIndex + 1;

    if (newIndex < images.length) {
      this.scrollView.current.scrollTo({
        x: newIndex * WIDTH,
        animated: true,
      });

      this.setState({ currentIndex: newIndex });
    } else {
      this.scrollView.current.scrollTo({
        x: 0,
        animated: true,
      });
      this.setState({ currentIndex: 0 });
    }
  };

  onScroll = (event) => {
    const { currentIndex } = this.state;
    const { contentOffset } = event.nativeEvent;

    const currentIndexs = Math.round(contentOffset.x / WIDTH);

    if (currentIndex !== currentIndexs) {
      this.setState({ currentIndexs });
    }
  };

  render() {
    const { currentIndex } = this.state;
    return (
      <Box>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          ref={this.scrollView}
          onScroll={this.onScroll}
        >
          {images.map((img, i) => (
            <Box
              key={i}
              position="relative"
              style={{ height: 130, width: WIDTH }}
            >
              <Image source={img} />
              <Box
                position="absolute"
                dir="row"
                style={{ height: 130, width: WIDTH }}
                align="end"
                justify="center"
                pb="xs"
              >
                {Array.from({ length: images.length }).map((_, index) => (
                  <Box
                    key={index}
                    bg={
                      currentIndex === index
                        ? 'blue'
                        : 'transparent'
                    }
                    style={{ borderWidth: 1, borderColor: 'grey' }}
                    circle={DOT_SIZE}
                    mx={DOT_SIZE / 2}
                  />
                ))}
              </Box>
            </Box>
          ))}
        </ScrollView>
      </Box>
    );
  }
}

export default Carsiuel;

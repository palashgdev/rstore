/* eslint linebreak-style: ["error", "windows"] */
import React from 'react';
import { Box, Text } from 'react-native-design-utility';
import { Image, TouchableOpacity } from 'react-native';

import { FontAwesome } from '@expo/vector-icons';
import { images } from '../constant/image';
import { theme } from '../constant/theme';

const bgColor = (type) => {
  switch (type) {
    case 'google':
      return 'googleBlue';

    case 'facebook':
      return 'facebookBlue';

    default:
      return 'white';
  }
};

const Login = ({ children, type, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <Box
      align="center"
      dir="row"
      w="80%"
      bg={bgColor(type)}
      self="center"
      p="2xs"
      shadow={1}
      radius="2xs"
      mb="sm"
    >
      <Box mr="sm">
        <Box
          bg="white"
          h={32}
          w={32}
          radius="2xs"
          style={{ position: 'relative' }}
          center
        >
          {type === 'google' && (
          <Image
            source={images.googleLogoIcon}
          />
          )}
          {type === 'facebook' && (
          <FontAwesome
            size={30}
            color={theme.color.facebookBlue}
            name="facebook"
            style={{ position: 'absolute', right: 7, bottom: -5 }}
          />
          )}
        </Box>
      </Box>
      <Text color="white" center>
        {children}
      </Text>
    </Box>
  </TouchableOpacity>
);

export default Login;

import React, { Component } from 'react';
import { Image } from 'react-native';
import { Box, Text } from 'react-native-design-utility';

class Product extends Component {
  state={ };

  render() {
    const {
      image, description, price, name,
    } = this.props;
    return (
      <Box>
        <Box>
          <Box>
            <Image source={image} />
          </Box>
          <Text>
            {name}
          </Text>
          <Text>
            {description}
          </Text>
          <Text>
            {price}
          </Text>
        </Box>
      </Box>
    );
  }
}

export default Product;

/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { Box } from 'react-native-design-utility';
import TabsItem from './TabItems';

class TabBar extends Component {
  state={}

  render() {
    const { navigation } = this.props;
    const { routes, index } = navigation.state;
    return (
      <Box h={60} bg="white" dir="row" shadow={0}>
        {routes.map((route, i) => (
          <TabsItem
            isActive={index === i}
            navigation={navigation}
            key={i}
            {...route}
          />
        ))}
      </Box>
    );
  }
}

export default TabBar;

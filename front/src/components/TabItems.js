import React,{ PureComponent } from "react";
import { Image,StyleSheet,TouchableOpacity } from "react-native";
import { Box, Text } from "react-native-design-utility";

import { tabBarIcons } from "../constant/image";

class TabsItem extends PureComponent{
  onPress = ()=> {
    this.props.navigation.navigate(this.props.routeName);
  }
  render(){
    const {routeName,isActive} = this.props;

    const icon = tabBarIcons[isActive?'active':'inactive'][routeName]
    return (
      <Box f={1}>
        <TouchableOpacity onPress={this.onPress} style={style.button}>
          <Box mb={3} pt={10}>
            <Image source={icon}/>
          </Box>
          <Box>
            <Text size="xs" ls={0.12} color="greyDark" lowercase>
              {routeName}
            </Text>
          </Box>
        </TouchableOpacity>
      </Box>
    )
  }
};

const style = StyleSheet.create({
  button:{
    flex:1,
    justifyContent:'center',
    alignItems:"center"
  }
})

export default TabsItem
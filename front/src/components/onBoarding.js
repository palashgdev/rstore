/* eslint linebreak-style: ["error", "windows"] */

import React from 'react';
import { Image } from 'react-native';
import { Box, Text } from 'react-native-design-utility';

import { images } from '../constant/image';

const OnBoarding = () => (
  <Box f={1} center>
    <Image source={images.logo} />
    <Box mb="sm">
      <Text size="xl">
            r
        <Text size="xl" color="blue">store</Text>
      </Text>
    </Box>
    <Box mb="sm">
      <Text>
        easy shoping!
      </Text>
    </Box>
  </Box>
);

export default OnBoarding;

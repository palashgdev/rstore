/* eslint linebreak-style: ["error", "windows"] */
/* eslint import/prefer-default-export:0 */


import { types } from 'mobx-state-tree';

export const ProductModel = types.model('productModel', {
  id: types.identifier,
  name: types.string,
  imageUrl: types.string,
  price: types.number,
  kgPrice: types.number,
  cartQty: 0,
  inCart: false,
});

/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { StatusBar, FlatList } from 'react-native';
import { Box } from 'react-native-design-utility';
import CategoryCard from '../components/CategoryCard';
import { theme } from '../constant/theme';
import Carsouel from '../components/Carsouel';
import ShoopingCart from '../components/ShoopingCart';

const NUMCOLUMNS = 2;
const categories = [
  {
    id: 1,
    title: 'Paper Products',
    image: require('../../assets/icon/ListIconActive.png'),
  },
  {
    id: 2,
    title: 'Electronics',
    image: require('../../assets/icon/ListIconActive.png'),
  },
  {
    id: 3,
    title: 'Grocery',
    image: require('../../assets/icon/ListIconActive.png'),
  },
  {
    id: 4,
    title: 'Kuch bhi',
    image: require('../../assets/icon/StoresIconActive.png'),
  },
  {
    id: 5,
    title: 'Shoes',
    image: require('../../assets/icon/StoresIconActive.png'),
  },
  {
    id: 6,
    title: 'Watches',
    image: require('../../assets/icon/StoresIconActive.png'),
  },
  {
    id: 7,
    title: 'Paper Products',
    image: require('../../assets/icon/StoresIconActive.png'),
  },
  {
    id: 8,
    title: 'Paper Products',
    image: require('../../assets/icon/StoresIconActive.png'),
  },
];

class Home extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Home',
    headerTitleStyle: {
      fontWeight: 'bold',
      flex: 1,
      textAlign: 'center',
    },
    headerRight: <ShoopingCart />,
  })

  render() {
    return (
      <Box f={1}>
        <StatusBar
          networkActivityIndicatorVisible={false}
          translucent
          showHideTransition="slide"
          animated
          backgroundColor={theme.color.white}
          barStyle="light-content"
        />
        <Box w={1} h={140} bg="white">
          <Carsouel />
        </Box>

        <Box f={1} bg="white">
          <FlatList
            data={categories}
            renderItem={({ item, index }) => {
              const style = {};
              if (index % NUMCOLUMNS < NUMCOLUMNS) {
                style.borderBottomWidth = 2;
                style.borderBottomColor = theme.color.greyLightest;
              }
              if (index % NUMCOLUMNS != 0) {
                style.borderLeftWidth = 2;
                style.borderLeftColor = theme.color.greyLightest;
              }

              return (
                <Box w={1 / NUMCOLUMNS} bg="white" h={120} style={style}>
                  <CategoryCard {...item} />
                </Box>
              );
            }}
            keyExtractor={(item) => String(item.id)}
            numColumns={NUMCOLUMNS}
          />
        </Box>
      </Box>
    );
  }
}

export default Home;

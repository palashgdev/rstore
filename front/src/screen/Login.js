/* eslint linebreak-style: ["error", "windows"] */

import React, { Component } from 'react';
import { Animated } from 'react-native';
import { Box } from 'react-native-design-utility';

import { inject } from 'mobx-react/native';

import OnBoarding from '../components/onBoarding';
import LoginButton from '../components/LoginButton';

import { FacebookApi } from '../api/facebook';
import { googleApi } from '../api/google';

const BoxAnimated = Animated.createAnimatedComponent(Box);

@inject('authstore')

class Login extends Component {
  state = {
    opacity: new Animated.Value(0),
    position: new Animated.Value(0),

  };

  componentDidMount() {
    Animated.parallel([
      this.positionAnimation(),
      this.opacityAnimation(),
    ]);
  }

  opacityAnimation = () => {
    const { opacity } = this.state;
    Animated.timing(opacity, {
      toValue: 1,
      duration: 200,
      delay: 100,
    }).start();
  }

  positionAnimation = () => {
    const { position } = this.state;
    Animated.timing(position, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }

  onGooglePress = async () => {
    try {
      const { authstore } = this.props;
      const token = await googleApi.loginAsync();

      await authstore.login(token, 'GOOGLE');
    } catch (error) {
      console.error(error);
    }
  }

  OnFaceBookPress = async () => {
    try {
      const { authstore } = this.props;
      const token = await FacebookApi.loginAsync();

      await authstore.login(token, 'FACEBOOK');
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { opacity, position } = this.state;

    const logoTranslate = position.interpolate({
      inputRange: [0, 1],
      outputRange: [150, 0],
    });

    return (
      <Box f={1} center>

        <BoxAnimated
          f={1}
          style={{
            transform: [
              {
                translateY: logoTranslate,
              },
            ],
          }}
        >
          <Box f={1} center>
            <OnBoarding />
          </Box>
        </BoxAnimated>

        <BoxAnimated w={1} f={0.9} style={{ opacity }}>
          <LoginButton
            onPress={this.onGooglePress}
            type="google"
          >
            Continue With Google
          </LoginButton>
          <LoginButton
            onPress={this.OnFaceBookPress}
            type="facebook"
          >
          Continue With Facebook
          </LoginButton>
        </BoxAnimated>
      </Box>
    );
  }
}

export default Login;

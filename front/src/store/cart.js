/* eslint linebreak-style: ["error", "windows"] */
/* eslint import/prefer-default-export:0 */

import { types } from 'mobx-state-tree';
import { ProductModel } from '../models/product';

export const cart = types.model('Shopingcart', {
  products: types.array(types.reference(ProductModel)),
});


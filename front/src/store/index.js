/* eslint linebreak-style: ["error", "windows"] */

import { AuthStore } from './Auth';
import { cart } from './cart';
import { ProductStore } from './products';
import { ProductModel } from '../models/product';

const authstore = AuthStore.create();

const Cart = cart.create({ products: [] });

const products = ProductStore.create({
  data: [
    ProductModel.create(
      {
        id: '1',
        name: 'kuch nhi',
        imageUrl: 'google.com',
        price: 10,
        kgPrice: 120,
      },
      {
        id: '1',
        name: 'ata pta',
        imageUrl: 'google.com',
        price: 10,
        kgPrice: 120,
      },
    ),
  ],
});


export const store = {
  authstore,
  Cart,
  products,
};


window.MobxStore = store;

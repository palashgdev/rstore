/* eslint linebreak-style: ["error", "windows"] */
/* eslint import/prefer-default-export:0 */


import { types } from 'mobx-state-tree';
import { ProductModel } from '../models/product';

export const ProductStore = types.model('ProductStore', {
  data: types.array(types.reference(ProductModel)),
});

